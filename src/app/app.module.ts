import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { TopBarComponent } from './top-bar/top-bar.component';

import { PersonagemDetailsComponent } from './personagem-details/personagem-details.component';
import { PersonagemListComponent } from "./personagem-list/personagem-list.component";
import { PersonagemService } from './personagem.service';
import { AboutComponent } from './about/about.component';
import { DataService } from './data.service';
import { CustomersListComponent } from './customers-list/customers-list.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', redirectTo: '/list', pathMatch: 'full'},
      { path: 'list', component: PersonagemListComponent },
      { path: 'personagens/:personagemId', component: PersonagemDetailsComponent },
      // { path: 'products/:productId', component: ProductDetailsComponent },
      // { path: 'cart', component: CartComponent },
      // { path: 'shipping', component: ShippingComponent },
      { path: 'about', component: AboutComponent },
    ])
  ],
  declarations: [
    AppComponent,
    TopBarComponent,
    // ProductListComponent,
    // ProductAlertsComponent,
    // ProductDetailsComponent,
    PersonagemDetailsComponent,
    PersonagemListComponent,
    CustomersListComponent,
    // CartComponent,
    // ShippingComponent,
    AboutComponent
  ],
  bootstrap: [ AppComponent ],
  providers: [
    // CartService,
    DataService,
    PersonagemService
  ]
})
export class AppModule { }


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/