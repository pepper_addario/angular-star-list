import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { personagens } from '../personagens';
import { PersonagemService } from '../personagem.service';

@Component({
  selector: 'app-personagem-details',
  templateUrl: './personagem-details.component.html',
  styleUrls: ['./personagem-details.component.css']
})
export class PersonagemDetailsComponent implements OnInit {
  personagem;
  constructor(
    private route: ActivatedRoute,
    private personagemService: PersonagemService
  ) { }

  addToCart(personagem) {
    window.alert('Your product has been added to the cart!');
    this.personagemService.addToCart(personagem);
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.personagem = personagens[+params.get('personagemId')];
    });
  }

}