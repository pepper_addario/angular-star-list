import { Component, OnInit, Input } from '@angular/core';
import { ICustomer } from '../interfaces';
import { PersonagemService } from '../personagem.service'

@Component({
  selector: 'cm-customers-list',
  templateUrl: './customers-list.component.html',
  styleUrls: ['./customers-list.component.css']
})
export class CustomersListComponent implements OnInit {

  @Input() customers: ICustomer[];
  starshipInfo;

  constructor(
    private PersonagemService: PersonagemService
  ) { }

  loading = true
  modalShow = false;

  btnAction(starship) {
    this.PersonagemService.getStarshipInfo(starship)
      .subscribe((data) => {
        this.starshipInfo = { ...data }
        this.modalShow = !this.modalShow;
        this.loading = !this.loading;
      });
  }

  closeModal() {
    this.modalShow = false;
  }

  ngOnInit() {
    this.loading = !this.loading;
  }

}
