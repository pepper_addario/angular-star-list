import { Component, OnInit } from '@angular/core';
import { PersonagemService } from '../personagem.service'

@Component({
  selector: 'app-personagem-list',
  templateUrl: './personagem-list.component.html',
  styleUrls: ['./personagem-list.component.css'],
  
})
export class PersonagemListComponent implements OnInit {
  personagens;
  loading = true

  constructor(
    private PersonagemService: PersonagemService
  ) { }

  share() {
    window.alert('The product has been shared!');
  }
  onNotify() {
    window.alert('You will be notified when the product goes on sale');
  }

  ngOnInit() {
    this.personagens = this.PersonagemService.getPersonagemInfo();
    this.loading = !this.loading;
  }
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/